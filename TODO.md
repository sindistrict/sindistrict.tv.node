### SinDistrict TODO List

- [x] Add better system logging.
- [ ] Add configuration page for adding Plex API info.
- [ ] Plex API authentication for user logins.
- [x] Fetch recently added media from each library.
- [x] Force trailing slashes to URL.
- [x] Add routes for media libraries.
- [ ] Add the ability to force library updates on-demand.
- [x] Fetch list of genres from all libraries.
- [ ] Not all movies and shows are showing up in the correct genre arrays.
- [ ] Alphabetize the order of genres in the menu.
- [x] Extract primary color from media banner.