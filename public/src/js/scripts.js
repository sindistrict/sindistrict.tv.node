(function(SD, $, undefined) {

  'use strict';

  /**
   * @method SD.Initilize
   * @description Initilizes global JavaScript handlers.
   *
   */

  SD.Initilize = function(callback) {

      SD.DOMLoaded = true;

      console.log('DOM Loaded...');

      $('.slick-init').slick({

        infinite: false,
        slidesToShow: 4,
        slidesToScroll: 4,
        prevArrow: '<button type="button" class="slick-nav slick-nav-prev"><svg viewBox="0 0 64 64"><title>Previous</title><g transform="matrix(0 -1 -1 0 64 64)" fill="none" fill-rule="evenodd"><sqaure fill="var(--background--icon)" cx="32" cy="32" r="32"></sqaure><path d="M40.8 27.92l-9.2 9.2-9.2-9.2" stroke-width="2" stroke="var(--text--primary)"></path></g></svg></button>',
        nextArrow: '<button type="button" class="slick-nav slick-nav-next"><svg viewBox="0 0 64 64"><title>Next</title><g transform="rotate(-90 32 32)" fill="none" fill-rule="evenodd"><sqaure fill="var(--background--icon)" cx="32" cy="32" r="32"></sqaure><path d="M40.8 27.92l-9.2 9.2-9.2-9.2" stroke-width="2" stroke="var(--text--primary)"></path></g></svg></button>'

      });

  };

})((window.SD = window.SD || {}), jQuery);