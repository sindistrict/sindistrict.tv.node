#!/usr/bin/env node


/**
 * System Module dependencies.
 */

var fs = require('fs');
var rimraf = require('rimraf');
var Colors = require('colors');


/**
 * Internal Module dependencies.
 */

var Helpers = require('./helpers');


var directory = 'cache';


module.exports = {


  directory: directory,


  cacheExists: function() {

    // Check if the cache dir exists.
    return fs.existsSync(directory);

  },


  cacheFileExists: function(file) {

    return (module.exports.cacheExists() && fs.existsSync(`${directory}/${file}`));

  },


  createCacheDirectory: function() {

    if(!module.exports.cacheExists()) {

      // Create the cache directory.
      fs.mkdirSync(directory);

    }

  },


  writeCacheFile: function(file, content) {

    // Create cache dir if it doesn't exist.
    module.exports.createCacheDirectory();

    // Create the cache file.
    fs.writeFileSync(`${directory}/${file}`, content);

  },


  readCacheFile: function(file) {

    if(module.exports.cacheFileExists(file)) {

      return JSON.parse(fs.readFileSync(`${directory}/${file}`, 'utf8'));

    }else{

      return {};

    }

  },


  clearCache: function(path = `${directory}`, callback) {

    if(module.exports.cacheExists()) {

      rimraf(`${directory}`, function() {

        console.log(Colors.red(`\nCache has been emptied`));

        Helpers.Callback(callback);

      });

    }else{

      console.log(Colors.red(`\nCache is already empty`));

      Helpers.Callback(callback);

    }

  }


}