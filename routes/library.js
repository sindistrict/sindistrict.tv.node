var express = require('express');
var router = express.Router();

/* GET library page. */
router.get('/', function(req, res, next) {

  var parts = req.originalUrl.split('/');
  parts = parts.filter(Boolean);

  console.log(parts);
  res.send(JSON.stringify(parts));

});

module.exports = router;
