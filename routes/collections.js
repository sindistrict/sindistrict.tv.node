var express = require('express');
var router = express.Router();

/* GET collections page. */
router.get('/', function(req, res, next) {

  res.render('pages/collections', {

    meta: {

      title: 'Collections'
      
    }

  });

});

module.exports = router;
