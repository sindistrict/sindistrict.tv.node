var express = require('express');
var router = express.Router();

/* GET genre page. */
router.get('/', function(req, res, next) {

  var parts = req.originalUrl.split('/');
  parts = parts.filter(Boolean);

  var genre = req.app.locals.plex.genres[parts[1]];

  res.render('pages/genre', {

    meta: {

      title: `Discover ${genre.title}`
      
    },

    genre: genre

  });

});

module.exports = router;
