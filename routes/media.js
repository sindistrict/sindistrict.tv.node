var express = require('express');
var router = express.Router();

/* GET media page. */
router.get('/', function(req, res, next) {

  var parts = req.originalUrl.split('/');
  parts = parts.filter(Boolean);

  var media = req.app.locals.plex.libraries[parts[0]].media[parts[1]];

  res.render('pages/media', {

    meta: {

      title: media.title
      
    },

    media: media

  });

});

module.exports = router;
