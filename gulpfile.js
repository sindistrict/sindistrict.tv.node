'use strict';
 
const gulp = require('gulp');
const scss = require('gulp-sass');
const prefix = require('gulp-autoprefixer')
const pretty = require('gulp-prettier')
 
scss.compiler = require('node-sass');
 
gulp.task('scss:compile', () => {
  return gulp.src('./public/src/scss/**/*.scss')
  .pipe(scss().on('error', scss.logError))
  .pipe(prefix({
    ignoreUnknownVersions: true,
    browsers: ['cover 95%']
  }))
  .pipe(gulp.dest('./public/dist/css'));
});


gulp.task('js:prettify', () => {
  return gulp.src('./public/src/js/**/*.js')
  .pipe(pretty({
    singleQuote: true,
    trailingComma: 'es5',
    bracketSpacing: true,
    arrowParens: 'always'
  }))
  .pipe(gulp.dest('./public/dist/js'));
})


gulp.task('files:watch', () => {
  gulp.watch('./public/src/scss/**/*.scss', gulp.series('scss:compile'));
  gulp.watch('./public/src/js/**/*.js', gulp.series('js:prettify'));
})

gulp.task('default', gulp.series(
  'scss:compile', 'js:prettify', 'files:watch'
))