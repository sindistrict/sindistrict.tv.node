/**
 * System Module dependencies.
 */

var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var app = express();

var CLI = require('clui');
var Spinner = CLI.Spinner;

var Colors = require('colors');


/**
 * Internal Module dependencies.
 */

var Plex = require('./exports/plex');
var Helpers = require('./exports/helpers');
var Cache = require('./exports/cache');


app.ServerReadyCallback = function(callback) {


  /**
   * Fetch the Plex media libraries.
   * Refresh the libraries every 60 seconds.
   */

  Plex.fetchEverything(function() {


    // Execute callback function.
    Helpers.Callback(callback);


    /**
     * Define globally accessible Jade variables.
     * These variables can be overwritten at the route level.
     */

    app.locals = {

      meta: {

        title: 'SINDISTRICT.TV',
        author: 'Sebastian Inman',
        description: 'This is just a test'

      },

      plex: {

        config: Plex.config,
        libraries: Plex.libraries,
        genres: Plex.genres,
        collections: Plex.collections,

      }

    };


    /**
     * Force the URL to have a trailing slash.
     * No actual reason for this, I just think it looks nicer
     * and keeps things more consistent.
     */

    app.use(function(req, res, next) {

      var test = /([^\/][(\w\s-_)]+.(js|css|jpg|png|gif|svg|eot|ttf|woff|woff2))$/.test(req.url);

      if (req.url.substr(-1) !== '/' && req.url.length > 1 && !test) {

        res.redirect(301, `${req.url}/`)

      }else{

        next();

      }

    });


    // view engine setup
    app.set('views', path.join(__dirname, 'views'));
    app.set('view engine', 'jade');

    app.use(express.json());
    app.use(express.urlencoded({ extended: false }));
    app.use(cookieParser());
    app.use(express.static(path.join(__dirname, 'public')));
    // app.use(logger('dev'));


    /**
     * Route Handlers.
     * Render page front-end based on the current URL.
     */

    app.use(`/`, require(`./routes/index`));
    app.use(`/collections/`, require(`./routes/collections`));
    app.use(`/genre/:genre/`, require(`./routes/genre`));

    Object.keys(Plex.libraries).map(function(library) {

      app.use(`/${library}/`, require(`./routes/library`));
      app.use(`/${library}/:media/`, require(`./routes/media`));

    });


    // catch 404 and forward to error handler
    app.use(function(req, res, next) {

      next(createError(404));
      
    });


    // error handler
    app.use(function(err, req, res, next) {

      // set locals, only providing error in development
      res.locals.message = err.message;
      res.locals.error = req.app.get('env') === 'development' ? err : {};

      // render the error page
      res.status(err.status || 500);
      res.render('pages/error');

    });


    var number = (Plex.config.options.refresh / 1000);
    var countdown = new Spinner(`Updating Plex libraries in ${number} seconds...  `, ['⣾','⣽','⣻','⢿','⡿','⣟','⣯','⣷']);

    countdown.start();

    setInterval(function () {

      number--;
      countdown.message(`Updating Plex libraries in ${number} seconds...  `);

      if (number === 0) {

        console.clear();

        Plex.fetchEverything(function() {

          // Reset the refresh timer.
          number = (Plex.config.options.refresh / 1000);
    
        }); 

      }
      
    }, 1000);


  });

};


module.exports = app;
